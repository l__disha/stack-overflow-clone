<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
	use HasFactory, Notifiable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name',
		'email',
		'password',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password',
		'remember_token',
	];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'email_verified_at' => 'datetime',
	];
	public function hasQuestionUpVote(Question $question)
	{
		return auth()->user()->votesQuestion()->where(['vote' => 1, 'vote_id' => $question->id, 'vote_type' => Question::class])->exists();
		//ands all condition
	}

	public function hasQuestionDownvote(Question $question)
	{
		return auth()->user()->votesQuestion()
									->where([
										'vote' => -1,
										'vote_id' => $question->id,
										'vote_type' => Question::class,
									])->exists();
	}

    public function hasVoteForQuestion(Question $question)
    {
        return $this->hasQuestionUpVote($question) || $this->hasQuestionDownVote($question);
    }

	public function questions()
	{
		return $this->hasMany(Question::class);
	}

	public function getAvatarAttribute()
	{
		$size = 40;
		$name = $this->name;
		return "https://ui-avatars.com/api/?name={$name}&rounded=true&size={$size}";
	}

	public function favourites(){
		return $this->belongsToMany(Question::class)->withTimestamps();
	}

	public function votesAnswer()
	{
		return $this->morphedByMany(Answer::class, 'vote')->withTimestamps();
	}

	public function votesQuestion()
	{
		return $this->morphedByMany(Question::class, 'vote')->withTimestamps();
	}
}
