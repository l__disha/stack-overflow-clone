<?php

namespace Database\Seeders;

use App\Models\Question;
use App\Models\User;
use App\Models\Answer;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        User::factory(5)->create()->each(function($user) {
            for($i=1; $i<rand(5, 10); ++$i) {
                // $user->question()->create(Question::factory()->make()->toArray());

                $user->questions()
                    ->saveMany(
                        Question::factory(rand(2, 5))->make()
                    )
                    ->each(function(Question $question) {
                        $question->answers()
                        ->saveMany(Answer::factory(rand(2, 6))->make());
                    });
            }
        });
    }
}
