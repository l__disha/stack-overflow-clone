<?php

use App\Http\Controllers\AnswerController;
use App\Http\Controllers\QuestionController;
use App\Http\Controllers\FavouritesController;
use App\Http\Controllers\VotesController;
use App\Models\Question;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::resource('questions', QuestionController::class)->except('show');
Route::get('/questions/{slug}', [QuestionController::class, 'show'])->name('questions.show');
Route::resource('questions.answers', AnswerController::class)->except('show', 'create', 'index');
Route::put('answers/{answer}/best-answer', [AnswerController::class, 'markBestAnswer'])->name('answers.best-answer');
Route::post('questions/{question}/favourite', [FavouritesController::class,  'store'])->name('questions.favourite');
Route::delete('questions/{question}/unfavourite', [FavouritesController::class,  'destroy'])->name('questions.unfavourite');
Route::post('questions/{question}/votes/{vote}', [VotesController::class,'voteQuestion'])->name('questions.vote');
// Route::post('answers/{answer}/votes/{vote}', [VotesController::class,'voteAnswer'])->name('answers.vote');
