@extends('layouts.app')

@section('styles')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.3.1/trix.min.css">
@endsection


@section('content')
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 ">
				<div class="card">
					<div class="card-header">
						<h1>{{$question->title}}</h1>
					</div>

					<div class="card-body">
						{!! $question->body !!}
					</div>

					<div class="card-footer">
						<div class="d-flex justify-content-between mr-3 mt-2">
							<div class="d-flex">
								<div class="">
									@auth
										<form action="{{route('questions.vote', [$question->id, 1])}}" method="POST">
											@csrf
											<button type="submit" title="Up Vote"
                                                class="btn {{auth()->user()->hasQuestionUpvote($question) ? 'text-dark' : 'text-black-50'}}">
												<i class="fa fa-caret-up fa-3x"></i>
											</button>
										</form>
									@else
                                        <a href="{{route('login')}}" class="vote-up text-black-50 d-block text-center" title="Up Vote">
                                            <i class="fa fa-caret-up fa-3x"></i>
                                        </a>
									@endauth

									<h4 class="votes-count text-muted text-center m-0">{{$question->votes_count}}</h4>

                                    @auth
                                    <form action="{{route('questions.vote', [$question->id, -1])}}" method="POST">
                                        @csrf
                                        <button type="submit" title="Down Vote" class="btn {{auth()->user()->hasQuestionDownvote($question) ? 'text-dark' : 'text-black-50'}}">
                                            <i class="fa fa-caret-down fa-3x"></i>
                                        </button>
                                    </form>
                                    @else
                                        <a href="{{route('login')}}" class="vote-down text-black-50 d-block text-center">
                                            <i class="fa fa-caret-down fa-3x"></i>
                                        </a>
                                    @endauth
								</div>

								<div class="ml-5 mt-3">
									{{-- <a href="" title="Mark as favourite" class="favourite text-dark text-center text-block">
										<i class="fa fa-star fa-2x"></i>
									</a> --}}

									@can('markAsFavourite', $question)
									<form action="{{route($question->is_favourite ? 'questions.unfavourite' : 'questions.favourite', $question->id)}}" method="POST">
										@csrf
										@if($question->is_favourite)
											@method('DELETE')
										@endif

										<button type="submit" class="btn {{$question->favourite_style}}">
											<i class="fa fa-star fa-2x"></i>
										</button>
									</form>
									@else
										<i class="fa fa-star-o text-success d-block fa-2x"></i>
									@endcan

									<h4 class="views-count text-muted text-center m-0 {{ $question->favourite_style}} text-center m-0">{{$question->favourites_count}}</h4>
								</div>
							</div>

							<div class="d-flex flex-column">
								<div class="text-muted mb-2 text-right">
									<p>Asked {{$question->created_date}}</p>
								</div>
								<div class="mb-2 d-flex">
									<div class="">
										<img src="{{$question->owner->avatar}}" alt="">
									</div>
								</div>
								<div class="mt- ml-2">
									{{$question->owner->name}}
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		@include('answers._index')
		@include('answers._create')
	</div>
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.3.1/trix.js"></script>
@endsection

