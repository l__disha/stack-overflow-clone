<div class="row mt-4">
    <div class="col-md-12 ">
        <div class="card">
            <div class="card-header">
                <h3 class="mt-0">{{Illuminate\Support\Str::plural('Answer', $question->answers_count)}}</h3>
            </div>

            <div class="card-body">
                @foreach ($question->answers as $answer)
                    {!! $answer->body !!}
                    <div class="d-flex justify-content-between mr-3 mt-2">
                        <div class="d-flex">
                            <div class="">
                                <a href="" class="vote-up text-black-50 d-block text-center" title="Up Vote">
                                    <i class="fa fa-caret-up fa-3x"></i>
                                </a>

                                <h4 class="votes-count text-muted text-center m-0">{{$answer->votes_count}}</h4>

                                <a href="" class="vote-down text-black-50 d-block text-center" title="Down Vote">
                                    <i class="fa fa-caret-down fa-3x"></i>
                                </a>
                            </div>

                            <div class="ml-5 mt-3">
                                @can('markAsbest', $answer)
                                    <form action="{{route('answers.best-answer', $answer->id)}}" method="POST">
                                        @csrf
                                        @method('PUT')
                                        <button type="submit" class="btn {{$answer->best_answer_style}}" title="Mark as best answer">
                                            <i class="fa fa-check fa-2x"></i>
                                        </button>
                                    </form>

                                @else
                                    @if($answer->is_best_answer)
                                        <i class="fa fa-check fa-2x d-block text-success mb-2"></i>
                                    @endif
                                @endcan

                                {{-- <h4 class="views-count text-muted text-center m-0">{{$answer->views_count}}</h4> --}}
                            </div>
                        </div>

                        <div class="d-flex">
                            @can('update', $answer)
                                <div class="mr-3">
                                    <a href="{{route('questions.answers.edit', [$question->id, $answer->id])}}" class="btn btn-outline-info">Edit</a>
                                </div>
                            @endcan

                            @can('delete', $answer)
                                <form action="{{route('questions.answers.destroy', [$question->id, $answer->id])}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-outline-danger" onclick="return confirm('Are you sure you want to delete?'">
                                        Delete
                                    </button>
                                </form>
                            @endcan
                        </div>

                        <div class="d-flex flex-column">
                            <div class="text-muted mb-2 text-right">
                                <p>Answered {{$answer->created_date}}</p>
                            </div>
                            <div class="mb-2 d-flex">
                                <div class="">
                                    <img src="{{$answer->author->avatar}}" alt="">
                                </div>
                            </div>
                            <div class="mt-2 ml-1">
                                {{$answer->author->name}}
                            </div>

                        </div>
                    </div>
                    <hr>
                @endforeach
            </div>
        </div>
    </div>
</div>
